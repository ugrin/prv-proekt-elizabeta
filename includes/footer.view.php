<section class="footer-bottom container-fluid">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 footer-wrap">
                <p>Made with
                    <i class="fas fa-heart red"></i> by
                    <img src="public/img/brainster_logo.png" class="footer-logo"> -
                    <a href="https://www.facebook.com/brainster.co/">Say Hi!</a>  - Terms</p>        
        </div>
    </div>
</section>