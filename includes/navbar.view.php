<nav class="navbar-default yellow-background">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button hamburger-wrap" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <i class="fa fa-bars my-bars"></i>			
            </button>
            <a class="navbar-left" href="index.php"><img src="public/img/brainster_logo.png" class="logo img-responsive"></a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li class="nav-item">
                    <a class="nav-link" href="http://codepreneurs.co/">Академија за Програмирање</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://marketpreneurs.co/akademija-za-marketing-programa/">Академија за Маркетинг</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="https://blog.brainster.co/">Блог</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link modal-opener" href="" data-toggle="modal" data-target="#myModal">Вработи наши студенти</a>
                </li>
            </ul>
        </div>
    </div>
</nav>