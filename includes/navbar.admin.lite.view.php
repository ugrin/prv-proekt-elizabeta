<nav class="navbar-default yellow-background">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button hamburger-wrap" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <i class="fa fa-bars my-bars"></i>			
            </button>
            <a class="navbar-left" href="index.php"><img src="public/img/brainster_logo.png" class="logo img-responsive"></a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right admin-navbar-right">
                <li class="admin-nav-item">
                    <a href="admin_panel.php?status=successLogin"><button class="btn-admin-head">Врати се назад</button></a>
                </li>
            </ul>
        </div>
    </div>
</nav>
