<?php

namespace MyProject;

?>

<!DOCTYPE html>
<html>

    <head>
        <?php
            include_once "includes/head.view.php";
        ?>
    </head>


    <body>
        <?php
            include_once "includes/navbar.view.php";
            include_once "includes/modal.view.php";
        ?>

        <div class="container">
            <div class="row text-centered">
                <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-8 col-xs-offset-2 form-padding">
                    <p class="text-centered text-enhanced">Добредојдовте</p>

                        <?php
                            if(isset($_GET['status'])) {
                                if($_GET['status']=='success') {
                                    echo "<p>".$_GET['msg']."</p>";
                                } else {
                                    echo "<p class='red'>Погрешно корисничко име или лозинка</p>";
                                }
                            }
                        ?>

                    <form method="POST" action="admin_verify.php">
                        <div class="form-group">
                            <input required type="text" name="username" id="username" class="input" placeholder="Корисничко име" />
                        </div>
                        <div class="form-group">
                            <input required type="password" name="password" id="password" class="input" placeholder="Лозинка" />
                        </div>
                        <div class="form-group">
                            <button type="submit" name="login" class="input input-btn input-btn-extra-margin">Влез</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <?php
            include_once "includes/footer.view.php";
        ?>

		<script src="scripts/jquery.js"></script>
		<script src="scripts/bootstrap.js"></script>

    </body>

</html>