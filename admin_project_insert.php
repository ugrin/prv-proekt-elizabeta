<?php
namespace MyProject;
require_once "Database/Connection.php";
require_once "Components/Project.php";
require_once 'Helpers/Redirection.php';

if (isset($_GET['status']) || $_POST['session'] == 'successLogin') {


$stmt = Project::getAll();
?>


<!DOCTYPE html>
<html>

<head>
    <?php
        include_once "includes/head.view.php";
    ?>
</head>

<body>

    <?php
        include_once "includes/navbar.admin.lite.view.php";

        if(isset($_GET['status']) && $_GET['status'] == 'saveError') {
                echo "<h3 class='text-centered'>Неуспешно внесен проект</h3>";
        }                 
    ?> 

    <div class="container ">
        <div class="row ">
            <div class="col-xs-8 col-xs-offset-2 col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4 form-padding">
                <form method="POST" action="edit.php">
                    <div class="form-group">
                        <input required type="url" name="link" class="input" placeholder="Линк до проектот" />
                    </div>
                    <div class="form-group">
                        <input required type="url" name="img_path" class="input" placeholder="URL за слика"/>
                    </div>
                    <div class="form-group">
                        <input required type="text" name="title" class="input" placeholder="Наслов"/>
                    </div>
                    <div class="form-group">
                        <input required type="text" name="subtitle" class="input" placeholder="Поднаслов"/>
                    </div>
                    <div class="form-group">
                        <?php
                        if (isset($_GET['status']) && $_GET['status'] == 'maxCaracters') {
                            echo "<p class='text-centered red'>Описот не смее да биде подолг од 200 карактери</p>";
                        } 
                        ?>
                        <textarea required class="input" name="description" maxlength="200" rows="5" cols="20"  placeholder="Краток опис"></textarea>
                    </div>
                    <div class="btn-block">
                        <input type="hidden" name="method" value="save"/>
                        <input type="submit" class="input input-btn" value="Внеси"> 
                    </div>              
                </form>
            </div>
            
        </div>
    </div>
      
    <?php
        include_once "includes/footer.view.php";
    ?>

    <script src="scripts/jquery.js"></script>
    <script src="scripts/bootstrap.js"></script>
</body>

</html>


<?php

} else {
    Redirection::redirect('index.php');
    die();
}

?>