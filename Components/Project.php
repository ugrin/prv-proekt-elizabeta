<?php

namespace MyProject;

class Project {
    private $link, $img_path, $title, $subtitle, $description;

    public function __construct($link, $img_path, $title, $subtitle, $description){
		$this->link = $link;
        $this->img_path = $img_path;
        $this->title = $title;
        $this->subtitle = $subtitle;
        $this->description = $description;
    }

    public function save() {
		$db = DB::connect();

		if (Validation::validateDescription($this->description)) {
			try {
				$sql = "INSERT INTO projects (link, img_path, title, subtitle, description) VALUES (?, ?, ?, ?, ?)";
				
				$stmt = $db->prepare($sql);

				return $stmt->execute([$this->link, $this->img_path, $this->title, $this->subtitle, $this->description]);
			} catch (Exception $e) {
				Redirection::redirect('admin_project_insert.php', ['status=saveError']);
				return false;
			}
		}
		else {
			Redirection::redirect('admin_project_insert.php', ['status=maxCaracters']);
			return false;
		}
	}
	
	public function update(Int $id) {
		$db = DB::connect();

		if (strlen($this->description) <= 200) {
			try {
				$sql = "UPDATE projects SET link = ?, img_path = ?, title = ?, subtitle = ?, description = ? WHERE id = ? LIMIT 1";
				$stmt = $db->prepare($sql);

				return $stmt->execute([$this->link, $this->img_path, $this->title, $this->subtitle, $this->description, $id]);
				
			} catch (Exception $e) {
				Redirection::redirect('admin_project_update.php', ['status=updateError', 'id='.$id]);
				return false;
			}
		}
		else {
			
			Redirection::redirect('admin_project_update.php', ['status=maxCaracters', 'id='.$id]);
				return false;
			}
		}

    
    public static function getAll() {
		$db = DB::connect();

		try {
			$sql = "SELECT * FROM projects";
			$stmt = $db->query($sql);
			return $stmt;
		} catch (Exception $e) {
			return false;
		}
    }   

    public static function find($id) {
		$db = DB::connect();

		try {
			$sql = "SELECT * FROM projects WHERE id = ?";
			$stmt = $db->prepare($sql);
			$stmt->execute([$id]);

			if($stmt->rowCount() == 1) {
				$row = $stmt->fetch();
				return $row;
			}
			else {
				header("Location: index.php");
				return false;
			}
				
		} catch (Exception $e) {
			return false;
		}

	}

	public static function delete($id) {
		$db = DB::connect();
	
		try {
			$sql = "DELETE FROM projects WHERE id = ? LIMIT 1";
			$stmt = $db->prepare($sql);
			$stmt->execute([$id]);
	
			if($stmt->rowCount() > 0) {
				return true;
			} 
			else {
				return false;
			}
	
		} catch (Exception $e) {
			return false;
		}
	}

}