<?php

namespace MyProject;

require_once 'Helpers/Redirection.php';

use MyProject\Redirection;

require_once "Database/Connection.php";
require_once "Components/Project.php";
require_once "Components/Company.php";


if (isset($_POST['method']) && $_POST['method'] == 'save') {
	$link = $_POST["link"];
	$img = $_POST["img_path"];
	$title = $_POST["title"];
	$subtitle = $_POST["subtitle"];
	$description = $_POST["description"];

	$project = new Project($link, $img, $title, $subtitle, $description);

	if ($project->save()) {
		Redirection::redirect('admin_panel.php', ['status=save']);

	} else {
		Redirection::redirect('admin_project_insert.php', ['status=saveError']);
	}

} elseif (isset($_POST['update'])) {
	$id = $_POST['update'];
	Redirection::redirect("admin_project_update.php", ['id='.$id]);

} elseif (isset($_POST['method']) && $_POST['method'] == 'store') {
	$link = $_POST["link"];
	$img = $_POST["img_path"];
	$title = $_POST["title"];
	$subtitle = $_POST["subtitle"];
	$description = $_POST["description"];
	$id = $_POST['id'];

	$project = new Project($link, $img, $title, $subtitle, $description);

	if ($project->update($id)) {
		Redirection::redirect('admin_panel.php', ['status=update']);
	} else {
		Redirection::redirect('admin_project_update.php', ['status=updateError', 'id='.$id]);
}

} elseif (isset($_POST['delete'])) {

	$id = $_POST['delete'];
	
	if (Project::delete($id)) {
		Redirection::redirect('admin_panel.php', ['status=delete']);
	} else {
		Redirection::redirect('admin_panel.php', ['status=deleteError']);
	}

} elseif (isset($_POST['method']) && $_POST['method'] == 'company') {

	$company_name = $_POST['company_name'];
	$email = $_POST['email'];
	$phone = $_POST['phone'];

	$company = new Company($company_name, $email, $phone);

	if ($company->save()) {
		Redirection::redirect('index.php', ['status=sent']);

	} else {
		Redirection::redirect('index.php', ['status=sentError']);
	}

}


